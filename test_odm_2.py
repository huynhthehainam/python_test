from asyncio import tasks
import requests
from datetime import datetime
import glob
import asyncio
from gql import gql, Client
from gql.transport.aiohttp import AIOHTTPTransport

AUTH_HOST = "https://auth.flighthub.api.mismart.ai"

WEB_ODM_IP = "localhost"
WEB_ODM_HOST = f"http://{WEB_ODM_IP}:5000"
# WEB_ODM_HOST = "https://webodm.api.mismart.ai"


async def upload_single_image(url, headers, image):
    image = image.replace("\\", "/")
    bytes = open(image, "rb")
    files = {"file": ("image.JPG", bytes, "image/jpg")}
    resp = requests.post(url=url, files=files, headers=headers)
    if resp.ok:
        print(f"Uploaded image: {image}")


class ODMTester:
    access_token = None
    project_id = None
    task_id = None

    def get_access_token(
        self, email: str = "huynhthehainam.mismart1@gmail.com", password: str = "123"
    ):
        headers = {}
        url = f"{AUTH_HOST}/auth/login"
        data = {"email": email, "password": password}

        resp = requests.post(url=url, json=data, headers=headers)
        if resp.ok:
            self.access_token = resp.json()["data"]["accessToken"]
            print(f"Get access_token: {self.access_token}")
        else:
            print(f"Error: {resp.json()}")

    def create_node(self, hostname="node1.webodm.mismart.ai", port=80):
        if self.access_token is None:
            return

        url = f"{WEB_ODM_HOST}/api/processing_nodes/"
        headers = {"Authorization": f"Bearer {self.access_token}"}
        data = {"hostname": hostname, "port": port}
        resp = requests.post(url=url, json=data, headers=headers)

        if resp.ok:
            self.project_id = resp.json()["data"]["id"]
            print(f"Created node: {self.project_id}")
        else:
            print(f"Error: {resp.json()}")

    def create_project(
        self, name: str = "Project test", description: str = "Only for testing"
    ):
        if self.access_token is None:
            return

        headers = {"Authorization": f"Bearer {self.access_token}"}

        mutation = gql(
            '''
                mutation {
                    create_project(data: { name: "fasfasf" }) {
                        id
                        name
                    }
                }
            '''
        )
        transport = AIOHTTPTransport(
            url=f'{WEB_ODM_HOST}/graphql', headers=headers)
        client = Client(transport=transport,
                        fetch_schema_from_transport=True)

        result = client.execute(mutation)
        data =  result['create_project']
        self.project_id = data['id']
        print(f'Created project {self.project_id}')

    def create_task(self):
        if self.project_id is None:
            return
        headers = {"Authorization": f"Bearer {self.access_token}"}
        aa  = '''
mutation {
  create_task(
    data: {
      name: "asfasf"
      options: [
        { name: "auto-boundary", value: true }
        { name: "dsm", value: true }
      ]
      project_id: '''  + str(self.project_id)+'''
      resize_to: 2048
    }
  ) {
    id
    name
  }
}
            '''
       
        mutation =  gql(
           aa
        )
        transport = AIOHTTPTransport(
            url=f'{WEB_ODM_HOST}/graphql', headers=headers)
        client = Client(transport=transport,
                        fetch_schema_from_transport=True)

        result = client.execute(mutation)
        data =  result['create_task']
        self.task_id = data['id']
        print(f'Created task {self.task_id}')

    def upload_data(self, path: str = "C:/Users/huynh/Desktop/Work/webodm_image_tst"):
        if self.task_id is None or self.project_id is None:
            return

        headers = {"Authorization": f"Bearer {self.access_token}"}

        url = f"{WEB_ODM_HOST}/api/tasks/{self.task_id}/upload"
        file_types = ['*.JPG']
        images = []
        for file_type in file_types:
            file_query = f"{path}/{file_type}"
            images.extend(glob.glob(file_query))
        loop = asyncio.get_event_loop()
        tasks = []
        for image in images:
            task = loop.create_task(upload_single_image(url, headers, image))
            tasks.append(task)
        loop.run_until_complete(asyncio.wait(tasks))

    def commit(self):
        if self.task_id is None or self.project_id is None:
            return
        headers = {"Authorization": f"Bearer {self.access_token}"}
        transport = AIOHTTPTransport(
            url=f'{WEB_ODM_HOST}/graphql', headers=headers)
        client = Client(transport=transport,
                        fetch_schema_from_transport=True)
        headers = {"Authorization": f"Bearer {self.access_token}"}
        
        aa =        ''' mutation {
commit_task(data: { task_id: "''' + str(self.task_id) + '''" }) {
    id
    name
    committed
  }
}
        '''
        mutation = gql(
            aa
        )
        result = client.execute(mutation)
        data= result['commit_task']

        print(f'Committed task {self.task_id}')


def test_odm():
    tester = ODMTester()
    tester.get_access_token(
        email="huynhthehainam.mismart1@gmail.com", password="Doyouloveme1029")
    # tester.create_node()
    tester.create_project()
    tester.create_task()
    tester.upload_data("data")
    tester.commit()


if __name__ == "__main__":
    test_odm()
