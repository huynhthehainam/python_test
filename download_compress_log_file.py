import requests

AUTH_HOST = "https://auth.flighthub.api.mismart.ai"
DRONE_HUB_HOST = 'https://dronehub.api.mismart.ai'


class LogFileTester:

    def get_access_token(
        self, email: str = "huynhthehainam.mismart@gmail.com", password: str = "123"
    ):
        headers = {}
        url = f"{AUTH_HOST}/auth/login"
        data = {"email": email, "password": password}

        resp = requests.post(url=url, json=data, headers=headers)
        if resp.ok:
            self.access_token = resp.json()["data"]["accessToken"]
            print(f"Get access_token: {self.access_token}")
        else:
            print(f"Error: {resp.json()}")
    def get_authorized_headers(self):
        if self.access_token:
            return {"Authorization": f"Bearer {self.access_token}"}

    def get_log_files(self):
        headers =  self.get_authorized_headers()

        device_id = 28  # one of 8, 23, 28, 29
        from_date = None  # from date
        to_date =  None # to date
        page_index = None # pagination
        page_size = None # pagination
        params = {
            'deviceID': device_id,
            'from': from_date,
            'to': to_date,
            'pageIndex': page_index,
            'pageSize': page_size
        
        }
        url = f'{DRONE_HUB_HOST}/logfiles/getzipfile'
        resp = requests.get(url, params=params, headers = headers)
        if resp.ok:
            path = 'compress.zip'
            print(f'download file at {path}')
            with open(path, 'wb') as f:
                f.write(resp.content)



if __name__ == '__main__':
    tester = LogFileTester()
    tester.get_access_token("huynhthehainam.mismart@gmail.com",'123456')
    tester.get_log_files()

