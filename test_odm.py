from asyncio import tasks
import requests
from datetime import datetime
import glob
import asyncio

AUTH_HOST = "https://auth.api.mismart.ai"

WEB_ODM_IP = "172.17.25.193"
WEB_ODM_HOST = f"http://{WEB_ODM_IP}:8000"
WEB_ODM_HOST = "https://webodm.api.mismart.ai"


async def upload_single_image(url, headers, image):
    image = image.replace("\\", "/")
    bytes = open(image, "rb")
    files = {"file": ("image.JPG", bytes, "image/jpg")}
    resp = requests.post(url=url, files=files, headers=headers)
    if resp.ok:
        print(f"Uploaded image: {image}")


class ODMTester:
    access_token = None
    project_id = None
    task_id = None

    def get_access_token(
        self, email: str = "huynhthehainam@gmail.com", password: str = "123"
    ):
        headers = {}
        url = f"{AUTH_HOST}/auth/login"
        data = {"email": email, "password": password}

        resp = requests.post(url=url, json=data, headers=headers)
        if resp.ok:
            self.access_token = resp.json()["data"]["accessToken"]
            print(f"Get access_token: {self.access_token}")
        else:
            print(f"Error: {resp.json()}")

    def create_node(self, hostname="node1.webodm.mismart.ai", port=80):
        if self.access_token is None:
            return

        url = f"{WEB_ODM_HOST}/api/processing_nodes/"
        headers = {"Authorization": f"Bearer {self.access_token}"}
        data = {"hostname": hostname, "port": port}
        resp = requests.post(url=url, json=data, headers=headers)

        if resp.ok:
            self.project_id = resp.json()["data"]["id"]
            print(f"Created node: {self.project_id}")
        else:
            print(f"Error: {resp.json()}")

    def create_project(
        self, name: str = "Project test", description: str = "Only for testing"
    ):
        if self.access_token is None:
            return

        url = f"{WEB_ODM_HOST}/api/projects/"
        headers = {"Authorization": f"Bearer {self.access_token}"}
        data = {"name": name, "description": description}
        resp = requests.post(url=url, json=data, headers=headers)

        if resp.ok:
            self.project_id = resp.json()["data"]["id"]
            print(f"Created project: {self.project_id}")
        else:
            print(f"Error: {resp.json()}")

    def create_task(self):
        if self.project_id is None:
            return
        url = f"{WEB_ODM_HOST}/api/projects/{self.project_id}/tasks/"
        headers = {"Authorization": f"Bearer {self.access_token}"}
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        data = {
            "name": f"Task of {dt_string}",
            "options": [{"name": "dsm", "value": True}],
            "processing_node": 1,
            "auto_processing_node": False,
            "partial": True,
            'resize_to': 2048
        }
        resp = requests.post(url=url, json=data, headers=headers)

        if resp.ok:
            self.task_id = resp.json()["data"]["id"]
            print(f"Created task: {self.task_id}")
        else:
            print(f"Error: {resp.json()}")

    def upload_data(self, path: str = "C:/Users/huynh/Desktop/Work/webodm_image_tst"):
        if self.task_id is None or self.project_id is None:
            return

        headers = {"Authorization": f"Bearer {self.access_token}"}

        url = f"{WEB_ODM_HOST}/api/projects/{self.project_id}/tasks/{self.task_id}/upload/"
        file_types = ['*.JPG']
        images = []
        for file_type in file_types:
            file_query = f"{path}/{file_type}"
            images.extend(glob.glob(file_query))
        loop = asyncio.get_event_loop()
        tasks = []
        for image in images:
            task = loop.create_task(upload_single_image(url, headers, image))
            tasks.append(task)
        loop.run_until_complete(asyncio.wait(tasks))

    def commit(self):
        if self.task_id is None or self.project_id is None:
            return
        url = f"{WEB_ODM_HOST}/api/projects/{self.project_id}/tasks/{self.task_id}/commit/"
        headers = {"Authorization": f"Bearer {self.access_token}"}
        resp = requests.post(url=url, headers=headers)
        if resp.ok:
            print(f"Committed")
        else:
            print(f"Error: {resp.json()}")


def test_odm():
    tester = ODMTester()
    tester.get_access_token(
        email="huynhthehainam.mismart@gmail.com", password="123")
    tester.create_node()
    tester.create_project()
    tester.create_task()
    tester.upload_data("C:/Users/huynh/OneDrive/data/100_0001")
    tester.commit()


if __name__ == "__main__":
    test_odm()
