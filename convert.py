print(
  "B\u1ea1n \u0111\u00e3 t\u1eeb ch\u1ed1i \u0111\u01a1n h\u00e0ng n\u00e0y tr\u01b0\u1edbc \u0111\u00f3."

  # actually any encoding support printable ASCII would work, for example utf-8
  .encode('utf8')

  # unescape the string
  # source: https://stackoverflow.com/a/1885197
  .decode('unicode-escape')

  # latin-1 also works, see https://stackoverflow.com/q/7048745
  .encode('iso-8859-1')

  # finally
  .decode('utf-8')
)
