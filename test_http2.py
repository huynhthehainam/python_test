from hyper.contrib import HTTP20Adapter

import requests

s = requests.Session()

s.mount('https://dronehub.api.mismart.ai', HTTP20Adapter())
data = {
    "userID": 13
}

r = s.post(url='https://dronehub.api.mismart.ai/customers/1/testuser', json=data)
print(r.status_code)
