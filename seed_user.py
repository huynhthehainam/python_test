import pandas as pd
import unidecode
import json
import requests
import math

api_domain = 'https://appsync.v2.api.mismart.ai/'
# api_domain = 'http://localhost:5000/'


def auth(username, password):
    data = {
        'username': username,
        'password': password
    }
    url = f'{api_domain}auth/login'
    resp = requests.post(url, json=data)
    if resp.ok:
        return resp.json()
    return


def create_organization(auth_resp, name, organization_type, ):
    data = {
        'name': name,
        'type': organization_type,
        'addcress': 'South side',

    }
    url = f'{api_domain}organizations'
    token = auth_resp['data']['accessToken']
    headers = {'Authorization': f'Bearer {token}'}
    resp = requests.post(url, json=data, headers=headers)
    if resp.ok:
        return resp.json()
    return


def create_user(auth_resp, username, password, is_admin, organization_id, role, email, name, is_silent=True):
    data = {
        'username': username,
        'password': password,
        'isAdmin': is_admin,
        'organizationID': organization_id,
        'role': role,
        'email': email,
        'name': name,
        'isSilent': is_silent
    }
    url = f'{api_domain}users'
    token = auth_resp['data']['accessToken']
    headers = {'Authorization': f'Bearer {token}'}
    resp = requests.post(url, json=data, headers=headers)
    if resp.ok:
        return resp.json()
    return


def is_NaN(string):
    return string != string


def get_first_character_of_every_word(string):
    words = string.split(' ')
    result = ''
    for word in words:
        if len(word) > 0:
            result += word[0]
    return result.lower()


def seed_user(auth_resp):
    orga_resp = None
    organizations = []
    xlsx = pd.ExcelFile('users.xlsx')
    for sheet_name in xlsx.sheet_names:
        sheet_name = 'Các tỉnh phía Nam'
        columns = xlsx.parse(sheet_name).columns
        converters = {column: str for column in columns}
        df = xlsx.parse(sheet_name, converters=converters)
        usernames = []
        passwords = []
        roles = []
        organization_type = 'Lab'
        for index, row in df.iterrows():
            role = row['Role']
            if role not in roles:
                roles.append(role)

        if 'PreAdmin' not in roles:
            organization_type = 'Company'
        # print(organization_type)
        for index, row in df.iterrows():
            print(row['Email'])
            if not is_NaN(row['Email']):
                words = row['Email'].split('@')
                auth('admin.mismart', '123')
                username = ''
                password = ''
                if len(words) > 1:
                    phone_number = str(row['Số điện thoại di động'])
                    if len(phone_number) > 3:
                        role = row['Role']
                        organization = row['Cơ quan']
                        username = f'{words[0]}.{phone_number[-3:]}.{get_first_character_of_every_word(organization)}.{role.lower()}'
                        password = f'0{phone_number}'
                        # print(f'username: {username} password: {password}')
                        is_admin = False

                        email = row['Email']
                        name = row['Tên']

                        if organization not in organizations:
                            orga_resp = None
                            orga_resp = create_organization(
                                auth_resp, organization, organization_type, )
                            organizations.append(organization)
                        if orga_resp is not None:
                            organization_id = orga_resp['data']['id']

                            is_silent = row['Is Silent'] == 'Yes'

                            user_resp = create_user(
                                auth_resp, username, password, is_admin, organization_id, role, email, name, is_silent)
                            if user_resp:
                                print(
                                    f'username: {username} password: {password} orga_id: {organization}')
            else:
                username = ''
                password = ''
            usernames.append(username)
            passwords.append(password)

        df['Username'] = usernames
        df['Password'] = passwords
        df.to_excel(f'{sheet_name}.xlsx',
                    sheet_name='Sheet_1')
        break


def seed_pesticide():
    active_ingredients = []
    names = []
    pesticides = []
    xlsx = pd.ExcelFile('THUOCBVTV.xlsx')
    links = []
    sheet_name = 'PL1 -TT10 -DM 2020'
    columns = xlsx.parse(sheet_name).columns
    converters = {column: str for column in columns}
    df = xlsx.parse(sheet_name, converters=converters)
    print(columns)

    for index, row in df.iterrows():

        if not is_NaN(row['TÊN THƯƠNG PHẨM (TRADE NAME)']):
            splitted_active_ingredients = row['HOẠT CHẤT THUỐC BẢO VỆ THỰC VẬT KỸ THUẬT (COMMON NAME)'].split(
                ' + ')
            name = row['TÊN THƯƠNG PHẨM (TRADE NAME)']
            target = row['ĐỐI TƯỢNG PHÒNG TRỪ (PEST/ CROP)']
            applicant = row['TỔ CHỨC ĐỀ NGHỊ ĐĂNG KÝ (APPLICANT)']
            pesticide = {
                'Name': name,
                'Target': target,
                'Applicant': applicant,
            }
            if name not in names:
                names.append(name)
                pesticides.append(pesticide)

            for active_ingredient in splitted_active_ingredients:
                links.append({
                    'ActiveIngredient': active_ingredient,
                    'Pesticide': name
                })
                if active_ingredient not in active_ingredients:
                    active_ingredients.append(active_ingredient)

    result = {
        'Pesticides': pesticides,
        'ActiveIngredients': active_ingredients,
        'Links': links,

    }
    with open('seed_chatbot.json', 'w') as f:
        json.dump(result, f)


if __name__ == '__main__':
    seed_pesticide()
    # auth_resp = auth('admin.mismart', '123')
    # if auth_resp is not None:
    #     seed_user(auth_resp)
