import pandas as pd
import requests


def test():
    url = 'https://webhook.cyberbot.vn:9443/mobile/app/613f2a9085c42d30ead05445'
    token = '613db008bf9aade8ed42aa3b'
    headers = {'authen_key': f'{token}'}
    xlsx = pd.ExcelFile('cauhoi.xlsx')
    sheet_name = 'KB Ver1'
    columns = xlsx.parse(sheet_name).columns
    converters = {column: str for column in columns}
    df = xlsx.parse(sheet_name, converters=converters)
    answers = []
    for index, row in df.iterrows():
        question = row['Câu hỏi']
        print(question)
        data = {
            'sender': {
                'account': 'demo',
                'firstName': 'Firstname',
                'lastName': 'Lastname'
            },
            'message': {
                'text': question,
                'payload': 'PAYLOAD',
                'timestamp': 1623204178000,
                'mid': '123456'
            }
        }
        resp = requests.post(url, json=data, headers=headers)
        answer = ''
        if resp.ok:
            json = resp.json()
            responses = json['responses']
            for response in responses:
                if response['type'] == 'text':
                    answer = response['text']
                    print(answer)
        answers.append(answer)
    df['Trả lời AI'] = answers
    df.to_excel(f'{sheet_name}.xlsx',
                sheet_name='Sheet_1')


if __name__ == '__main__':
    test()
