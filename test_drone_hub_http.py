import json
import time

import requests


class DroneHubTester:
    host = 'https://dronehub.api.mismart.ai'
    locations = [{'lat': 20.991331, 'lng': 105.803306},
                 {'lat': 20.991229750000002, 'lng': 105.80338575},
                 {'lat': 20.991128500000002, 'lng': 105.8034655},
                 {'lat': 20.991027250000002, 'lng': 105.80354525},
                 {'lat': 20.990926, 'lng': 105.803625},
                 {'lat': 20.990799000000003, 'lng': 105.8036865},
                 {'lat': 20.990672, 'lng': 105.803748},
                 {'lat': 20.990544999999997, 'lng': 105.8038095},
                 {'lat': 20.990418, 'lng': 105.803871},
                 {'lat': 20.9903025, 'lng': 105.803923},
                 {'lat': 20.990187, 'lng': 105.80397500000001},
                 {'lat': 20.9900715, 'lng': 105.804027},
                 {'lat': 20.989956, 'lng': 105.804079},
                 {'lat': 20.989933999999998, 'lng': 105.80404175}, {'lat': 20.989912, 'lng': 105.80400449999999}, {'lat': 20.989890000000003, 'lng': 105.80396725}, {'lat': 20.989868, 'lng': 105.80393}, {'lat': 20.98985825, 'lng': 105.80391699999998}, {'lat': 20.9898485, 'lng': 105.80390399999999}, {'lat': 20.98983875, 'lng': 105.803891}, {'lat': 20.989829, 'lng': 105.803878}, {'lat': 20.989776, 'lng': 105.80379475}, {'lat': 20.989722999999998, 'lng': 105.80371149999999}, {'lat': 20.989669999999997, 'lng': 105.80362825}, {'lat': 20.989617, 'lng': 105.803545}, {'lat': 20.989592000000002, 'lng': 105.8035035}, {'lat': 20.989567, 'lng': 105.803462}, {'lat': 20.989542, 'lng': 105.8034205}, {'lat': 20.989517, 'lng': 105.803379}, {'lat': 20.98949125, 'lng': 105.80333125000001}, {'lat': 20.9894655, 'lng': 105.8032835}, {'lat': 20.989439750000003, 'lng': 105.80323575}, {'lat': 20.989414, 'lng': 105.803188}, {'lat': 20.989383750000002, 'lng': 105.80314100000001}, {'lat': 20.9893535, 'lng': 105.803094}, {'lat': 20.989323249999998, 'lng': 105.80304699999999}, {'lat': 20.989293, 'lng': 105.803}, {'lat': 20.9892685, 'lng': 105.8029595}, {'lat': 20.989244, 'lng': 105.802919}, {'lat': 20.989219499999997, 'lng': 105.80287849999999}, {'lat': 20.989195, 'lng': 105.802838}, {'lat': 20.989182, 'lng': 105.80278874999999}, {'lat': 20.989168999999997, 'lng': 105.8027395}, {'lat': 20.989155999999998, 'lng': 105.80269025}, {'lat': 20.989143, 'lng': 105.802641}, {'lat': 20.989154499999998, 'lng': 105.80258375}, {'lat': 20.989165999999997, 'lng': 105.8025265}, {'lat': 20.989177499999997, 'lng': 105.80246925}, {'lat': 20.989189, 'lng': 105.802412}, {'lat': 20.9891955, 'lng': 105.802373}, {'lat': 20.989202, 'lng': 105.802334}, {'lat': 20.9892085, 'lng': 105.802295}, {'lat': 20.989215, 'lng': 105.802256}, {
        'lat': 20.989222750000003, 'lng': 105.8022015}, {'lat': 20.9892305, 'lng': 105.80214699999999}, {'lat': 20.98923825, 'lng': 105.80209249999999}, {'lat': 20.989246, 'lng': 105.802038}, {'lat': 20.98925475, 'lng': 105.80198525}, {'lat': 20.9892635, 'lng': 105.80193249999999}, {'lat': 20.98927225, 'lng': 105.80187975}, {'lat': 20.989281, 'lng': 105.801827}, {'lat': 20.989295, 'lng': 105.8018085}, {'lat': 20.989309, 'lng': 105.80179000000001}, {'lat': 20.989323, 'lng': 105.8017715}, {'lat': 20.989337, 'lng': 105.801753}, {'lat': 20.9893765, 'lng': 105.80180175000001}, {'lat': 20.989416, 'lng': 105.8018505}, {'lat': 20.9894555, 'lng': 105.80189924999999}, {'lat': 20.989495, 'lng': 105.801948}, {'lat': 20.989528749999998, 'lng': 105.80198775}, {'lat': 20.989562499999998, 'lng': 105.80202750000001}, {'lat': 20.989596249999998, 'lng': 105.80206725000001}, {'lat': 20.98963, 'lng': 105.802107}, {'lat': 20.9896675, 'lng': 105.8021425}, {'lat': 20.989705, 'lng': 105.802178}, {'lat': 20.9897425, 'lng': 105.8022135}, {'lat': 20.98978, 'lng': 105.802249}, {'lat': 20.98983125, 'lng': 105.80221399999999}, {'lat': 20.9898825, 'lng': 105.802179}, {'lat': 20.98993375, 'lng': 105.802144}, {'lat': 20.989985, 'lng': 105.802109}, {'lat': 20.990026999999998, 'lng': 105.8020875}, {'lat': 20.990069, 'lng': 105.802066}, {'lat': 20.990111, 'lng': 105.8020445}, {'lat': 20.990153, 'lng': 105.802023}, {'lat': 20.9901995, 'lng': 105.80199300000001}, {'lat': 20.990246, 'lng': 105.801963}, {'lat': 20.9902925, 'lng': 105.80193299999999}, {'lat': 20.990339, 'lng': 105.801903}, {'lat': 20.990374, 'lng': 105.801952}, {'lat': 20.990409, 'lng': 105.80200099999999}, {'lat': 20.990444, 'lng': 105.80205}, {'lat': 20.990479, 'lng': 105.802099}, {'lat': 20.9905335, 'lng': 105.80217725}, {'lat': 20.990588000000002, 'lng': 105.8022555}, {'lat': 20.9906425, 'lng': 105.80233375}, {'lat': 20.990697, 'lng': 105.802412}, {'lat': 20.9907465, 'lng': 105.8024905}, {'lat': 20.990796, 'lng': 105.802569}, {'lat': 20.9908455, 'lng': 105.8026475}, {'lat': 20.990895, 'lng': 105.802726}, {'lat': 20.99092125, 'lng': 105.80276625}, {'lat': 20.990947499999997, 'lng': 105.8028065}, {'lat': 20.99097375, 'lng': 105.80284675}, {'lat': 20.991, 'lng': 105.802887}, {'lat': 20.99103375, 'lng': 105.80294475}, {'lat': 20.9910675, 'lng': 105.80300249999999}, {'lat': 20.99110125, 'lng': 105.80306024999999}, {'lat': 20.991135, 'lng': 105.803118}]
    tokens = ['eFWgpaRccWYCsx2B7j9gXecbZpdhQJyh','JKC1wGlBN4aq1LPhnoTAoIWYlm96gHJQ']
    paths = {
        'get_device_access_token': '/Auth/GenerateDeviceToken',
        'upload_telemetry': '/AuthorizedDevices/me/TelemetryRecords'
    }
    access_tokens = []

    def __init__(self) -> None:
        self.get_access_tokens()
        # data = []
        # data.append(self.locations[0])
        # for i in range(1, len(self.locations)):
        #     new_lat = (self.locations[i]['lat'] + self.locations[i-1]['lat'])/2
        #     new_lng = (self.locations[i]['lng'] + self.locations[i-1]['lng'])/2
        #     data.append({'lat': new_lat, 'lng': new_lng})
        #     data.append(self.locations[i])
        # print(data)
        pass

    def get_access_tokens(self):
        path = self.paths['get_device_access_token']
        url = f'{self.host}{path}'

        for token in self.tokens:
            data = {
                'deviceToken': token
            }

            resp = requests.post(url=url, json=data)

            if resp.ok:
                access_token = resp.json()['data']['accessToken']
                self.access_tokens.append(access_token)
                print(f'Get access token: {access_token}')
            else:
                print(f'Cannot get access token from {token}')

    def upload_telemetry(self):
        offset_lat = 20.991331 - 10.851782
        offset_lng = 105.803306 - 106.779229
      
        direction = 0
        data = []
        for i in range(len(self.locations)):
            if i%5  ==0:
                for access_token in self.access_tokens:
                    headers = {
                        'Authorization': f'Bearer {access_token}'
                    }
                    self.upload(headers, data)
                data = []

            location =  self.locations[i]
            lat = location['lat'] - offset_lat
            lng = location['lng'] - offset_lng
            direction += 10
            if direction >= 360:
                direction = direction - 360
            
            item = {
                'latitude': lat,
                'longitude': lng,
                'direction': direction
            }
            data.append(item)
        for access_token in self.access_tokens:
            headers = {
                'Authorization': f'Bearer {access_token}'
            }
            self.upload(headers, data)


    def upload(self, headers, data):
        if len(data)>0:
            path = self.paths['upload_telemetry']
            url = f'{self.host}{path}'
            resp = requests.post(url=url, headers=headers, json={'data':data})
            if resp.ok:
                print(f'upload data: {data}')
            else:
                print(f'Failed {resp.json()}')
            time.sleep(5)
            

if __name__ == '__main__':
    tester = DroneHubTester()
    tester.upload_telemetry()
