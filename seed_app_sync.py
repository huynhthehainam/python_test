

import os

from oauth2client.service_account import ServiceAccountCredentials
from oauth2client import client, GOOGLE_TOKEN_URI, GOOGLE_REVOKE_URI
from googleapiclient.discovery import build
import json
import requests
from enum import Enum
import pandas as pd

import httplib2


class ServiceMode(Enum):
    VIEW = 1
    UPLOAD = 2


API_URL = 'https://appsync.v2.api.mismart.ai'
CLIENT_ID = '118246236138-vbu9odv6d30a7tgcmj11at99ar6cofee.apps.googleusercontent.com'
CLIENT_SECRET = 'zbbOKkq_WL3C_WziXzXrVRhG'
REFRESH_TOKEN = '1//048c5SLMO2qLPCgYIARAAGAQSNwF-L9IraZRHl5k2Ng_KBAlZ0Tqit_xgOMuBxXZUv-ARkm2GRKCiODdhDoTwmFSFtflVyBsBbQk'


class AuthService:
    username = 'admin.mismart'
    password = '123'
    url = f'{API_URL}/auth/login'

    def get_token(self):
        data = {'username': self.username, 'password': self.password}
        resp = requests.post(self.url, json=data)
        if resp.ok:
            return resp.json()['data']


class GoogleDriveService:

    json_file_name = 'key.json'
    scopes = ['https://www.googleapis.com/auth/drive.file',
              'https://www.googleapis.com/auth/drive']

    prefix = 'MiSmartSync'
    root_folder_name = 'MiSmart'
    upload_root_folder_name = 'MiSmartUpload'
    root = None
    upload_root = None
    drive = None
    credentials = None

    mode = ServiceMode.VIEW

    def set_mode(self, mode: ServiceMode):
        self.mode = mode

    def __init__(self, mode: ServiceMode = ServiceMode.VIEW):
        self.mode = mode

    def get_cred(self):
        if self.credentials is None:
            self.credentials = client.OAuth2Credentials(
                access_token=None,  # set access_token to None since we use a refresh token
                client_id=CLIENT_ID,
                client_secret=CLIENT_SECRET,
                refresh_token=REFRESH_TOKEN,
                token_expiry=None,
                token_uri=GOOGLE_TOKEN_URI,
                user_agent=None,
                revoke_uri=GOOGLE_REVOKE_URI)

            # refresh the access token (optional)
            self.credentials.refresh(httplib2.Http())

        return self.credentials

    def get_drive(self):
        if self.drive is None:
            self.drive = build('drive', 'v3', credentials=self.get_cred())

        return self.drive

    def show_files_map(self):
        drive = self.get_drive()
        if drive is not None:
            while True:
                folders = self.get_folders()
                print(folders)

    def get_file_info(self, file_id):
        drive = self.get_drive()
        if drive is not None:
            info = drive.files().get(
                fileId=file_id, fields='id,name,parents,mimeType,description').execute()
            if info is not None:
                return info

    def get_folders(self, parent_id=None):
        if parent_id is None:
            parent_id = self.get_root()['id']
        q = f"mimeType = 'application/vnd.google-apps.folder'"
        q += f" and '{parent_id}' in parents"
        drive = self.get_drive()
        if drive is not None:
            result = drive.files().list(
                q=q, fields='files(id, name, parents, mimeType)', pageSize=1000).execute()
            folders = result['files']
            return folders

    def get_finished_files(self, parent_id: str):
        q = f"(mimeType ='image/jpeg' or mimeType ='image/png' )and '{parent_id}' in parents and name contains 'Finished'"
        drive = self.get_drive()
        if drive is not None:
            result = drive.files().list(
                q=q, fields='files(id, name, mimeType)', pageSize=1000).execute()
            files = result['files']
            return files

    def get_root(self):
        if self.mode == ServiceMode.VIEW:
            if self.root is None:
                self.root = self.find_root_file()
            return self.root
        else:
            if self.upload_root is None:
                self.upload_root = self.find_upload_root_file()
            return self.upload_root

    def find_upload_root_file(self):
        q = f"mimeType = 'application/vnd.google-apps.folder' and name = '{self.upload_root_folder_name}'"
        drive = self.get_drive()
        if drive is not None:
            result = drive.files().list(q=q).execute()
            files = result['files']
            root = None
            for file in files:
                if file['name'] == self.upload_root_folder_name:
                    return file

    def find_root_file(self):
        q = f"mimeType = 'application/vnd.google-apps.folder' and name = '{self.root_folder_name}'"
        drive = self.get_drive()
        if drive is not None:
            result = drive.files().list(q=q).execute()
            files = result['files']
            root = None
            for file in files:
                if file['name'] == self.root_folder_name:
                    return file

    def remove_all(self):
        drive = self.get_drive()
        if drive is not None:
            q = "mimeType = 'application/vnd.google-apps.folder'"
            result = drive.files().list(q=q).execute()
            files = result['files']
            for file in files:
                print(f"Removing: {file['name']} - {file['id']}")
                try:
                    drive.files().delete(fileId=file['id']).execute()
                except:
                    print(f"Failed: {file['name']} - {file['id']}")

    def create_folder(self, name: str, parent_id: str = None):
        drive = self.get_drive()
        if drive is not None:
            if parent_id is None:
                root = self.get_root()
                if root is not None:
                    parent_id = root['id']
            if parent_id is not None:
                file_metadata = {
                    'name': f'{self.prefix}_{name}',
                    'mimeType': 'application/vnd.google-apps.folder',
                    'parents': [parent_id]
                }
                file = drive.files().create(body=file_metadata, fields='id, name').execute()
                return file

    def find_by_name_and_parent(self, name: str, parent_id: str = None):
        if parent_id is None:
            parent_id = self.get_root()['id']
        name = f"{self.prefix}_{name}"
        q = f"mimeType = 'application/vnd.google-apps.folder'"
        if name is not None:
            q += f" and name = '{name}'"
        q += f" and '{parent_id}' in parents"
        drive = self.get_drive()
        if drive is not None:
            result = drive.files().list(
                q=q, fields='files(id, name, parents, mimeType)', pageSize=1000).execute()
            folders = result['files']
            # for folder in folders:
            #     print(folder['name'])
            #     if folder['name'] == name:

            #         return folder

            if len(folders) == 1:
                return folders[0]
        return None

    def find_by_link(self, link: str):
        drive = self.get_drive()
        if drive is not None:
            words = link.split('/')
            words
            parent = None
            folder = None
            is_valid = True
            for word in words:
                folder = None
                if parent is None:
                    folder = self.find_by_name_and_parent(word)
                else:
                    folder = self.find_by_name_and_parent(word, parent['id'])

                if folder:
                    if folder['name'] == f'{self.prefix}_{word}':
                        parent = folder
                    else:
                        is_valid = False
                        print(f'Failed: {link}, part: {word}')
                        break
                else:
                    print(f'Failed: {link}, part: {word}')
                    is_valid = False
                    break
            if is_valid:
                return folder
            else:
                return None
        return None


def path_hierarchy(path):
    hierarchy = {
        'type': 'folder',
        'name': os.path.basename(path),
        'path': path,
    }
    hierarchy['children'] = [
        path_hierarchy(os.path.join(path, contents))
        for contents in os.listdir(path) if os.path.isdir(os.path.join(path, contents))
    ]

    return hierarchy


def create_tree_json(path, file_name):
    tree = path_hierarchy(path)
    with open(file_name, 'w') as f:
        json.dump(tree, f)


def execute_tree(service: GoogleDriveService, tree, parent_id=None, first_layter=False):
    parent = None
    if not first_layter:
        parent = service.create_folder(tree['name'], parent_id)
        print(f"Created folder: {parent['name']} with id {parent['id']}")
    if len(tree['children']) > 0:
        current_parent_id = None
        if parent:
            current_parent_id = parent['id']
        for child in tree['children']:
            execute_tree(service, child, current_parent_id)


def generate_mapping_links():
    service = GoogleDriveService()
    data = None
    final_links = []
    failed_links = []
    with open('dataset_sv.json', 'r', encoding="utf8") as f:
        data = json.load(f)
    ok = True
    if (data is not None):
        links = data['Data']
        i = 0
        total = len(links)
        for sample in links:
            i += 1
            print(f'Created {i}/{total}')
            # if i < 47:
            #     continue
            long_path = sample['LongPath']
            service.set_mode(mode=ServiceMode.VIEW)
            view_folder = service.find_by_link(long_path)
            short_path = sample['ShortPath']
            service.set_mode(mode=ServiceMode.UPLOAD)
            upload_folder = service.find_by_link(short_path)
            if view_folder and upload_folder:
                final_link = {
                    'ViewID': view_folder['id'],
                    'UploadID': upload_folder['id']
                }
                final_links.append(final_link)
            else:
                ok = False
                failed_links.append({
                    'LongPath': long_path,
                    'ShortPath': short_path
                })
                failed_modes = []
                if not view_folder:
                    failed_modes.append(ServiceMode.VIEW)
                if not upload_folder:
                    failed_modes.append(ServiceMode.UPLOAD)
                print(
                    f'Failed: {failed_modes} Long - {long_path}, Short - {short_path}')
                break
    if ok:
        with open('final.json', 'w', encoding="utf8") as f:
            json.dump({'Ok': final_links, 'Fails': failed_links}, f)
        # for l in data['Data']:
        # long_path = l['LongPath']
        # short_path = l['ShortPath']


def callback(request_id, response, exception):
    if exception:
        # Handle error
        print(exception)
    else:
        print("Permission Id: %s" % response.get('id'))


def get_links(token):
    links = []
    access_token = token['accessToken']
    headers = {
        'Authorization': f'Bearer {access_token}'
    }

    resp = requests.get(f'{API_URL}/links', headers=headers)
    if resp.ok:
        return resp.json()['data']


def main():

    # folder_path = 'D:/Drive test/data/Long'

    # tree = path_hierarchy(folder_path)
    # if tree is not None:
    #     service = GoogleDriveService(mode=ServiceMode.VIEW)
    #     execute_tree(service, tree, None, True)

    # folder_path = 'D:/Drive test/data/Short'

    # tree = path_hierarchy(folder_path)
    # if tree is not None:
    #     service = GoogleDriveService(mode=ServiceMode.UPLOAD)
    #     execute_tree(service, tree, None, True)

    # generate_mapping_links()
    service = GoogleDriveService(mode=ServiceMode.UPLOAD)
    # service.show_files_map()
    # about = service.get_drive().about().get(fields='storageQuota,user/displayName').execute()
    # print(about)

    auth_service = AuthService()
    token = auth_service.get_token()
    links = get_links(token)
    data = []
    i = 0
    for link in links:
        i += 1
        # if i == 10:
        #     break
        parent_id = link['uploadID']

        files = service.get_finished_files(parent_id)
        organization_names = []
        counts = []
        texts = []
        if len(files) > 0:
            for google_file in files:
                name = google_file['name']
                words = name.split('-')
                organization_name = words[0]
                index = -1

                try:
                    index = organization_names.index(organization_name)
                except:
                    pass
                if index >= 0:
                    counts[index] += 1
                else:
                    organization_names.append(organization_name)
                    counts.append(1)
            for idx, organization_name in enumerate(organization_names):
                texts.append(f'{organization_name}: {counts[idx]}')

        levels = []
        while parent_id is not None:
            info = service.get_file_info(parent_id)
            parents = info.get('parents', [])
            levels.append(info['name'])
            if len(parents) > 0:
                parent_id = parents[0]
            else:
                parent_id = None
        levels.reverse()
        print(levels)

        data.append({
            'folder': '/'.join(levels),
            'result': ', '.join(texts)
        })
    texts = []
    rows = []
    for ele in data:
        row = [ele['folder'], ele['result'] if ele['result'] != '' else 0]
        rows.append(row)

    df = pd.DataFrame(rows, columns=['Folder', 'Result'])
    df.to_excel("output.xlsx",
                sheet_name='Sheet_name_1')


if __name__ == '__main__':
    main()
