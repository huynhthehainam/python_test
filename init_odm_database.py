import psycopg2

settings = {
    'user': 'postgres',
    'password': 'mismart123',
    'host': 'localhost',
    'port': 5432,
    'database': 'webodm_dev'
}

connection = psycopg2.connect(
    user=settings['user'],
    password=settings['password'],
    host=settings['host'],
    port=settings['port'],
)
connection.autocommit = True
cursor = connection.cursor()
creating_database_queries = [
    "DROP DATABASE IF EXISTS webodm_dev",
    "CREATE database webodm_dev",
]
creating_extension_queries = [
    "CREATE EXTENSION postgis",
    "CREATE EXTENSION postgis_raster",
    "CREATE EXTENSION postgis_topology",
    "CREATE EXTENSION postgis_sfcgal",
    "CREATE EXTENSION fuzzystrmatch",
    "CREATE EXTENSION address_standardizer",
    "CREATE EXTENSION address_standardizer_data_us",
    "CREATE EXTENSION postgis_tiger_geocoder",
    "ALTER SYSTEM SET postgis.enable_outdb_rasters TO True",
    "ALTER SYSTEM SET postgis.gdal_enabled_drivers TO 'GTiff'",
]

for query in creating_database_queries:
    print(f"Executing query: {query}")
    cursor.execute(query)


if connection:
    cursor.close()
    connection.close()

    print("PostgreSQL connection is closed")


connection = psycopg2.connect(
    user=settings['user'],
    password=settings['password'],
    host=settings['host'],
    port=settings['port'],
    database=settings['database'],
)
connection.autocommit = True
cursor = connection.cursor()
for query in creating_extension_queries:
    print(f"Executing query: {query}")
    cursor.execute(query)

if connection:
    cursor.close()
    connection.close()

    print("PostgreSQL connection is closed")
