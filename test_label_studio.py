import requests
import random


class LabelStudioTester():
    def __init__(self):
        self.hostname = 'http://localhost:5000'
        self.auth_hostname = 'https://auth.staging.api.mismart.ai'
        self.admin_account = {
            'email': 'huynhthehainam.mismart@gmail.com',
            'password': 'Doyouloveme1029'
        }
        self.accounts = [

            {
                'email': 'nam1@yopmail',
                'password': '123'
            },
            {
                'email': 'nam2@yopmail',
                'password': '123'
            },
            {
                'email': 'nam3@yopmail',
                'password': '123'
            },
            {
                'email': 'nam4@yopmail',
                'password': '123'
            },
            {
                'email': 'nam5@yopmail',
                'password': '123'
            },
            {
                'email': 'nam6@yopmail',
                'password': '123'
            },
            {
                'email': 'nam7@yopmail',
                'password': '123'
            },
            {
                'email': 'nam8@yopmail',
                'password': '123'
            },
            {
                'email': 'nam9@yopmail',
                'password': '123'
            },
            {
                'email': 'nam10@yopmail',
                'password': '123'
            },
        ]
        self.get_admin_token()
        self.get_tokens()

    def get_admin_token(self):
        data = self.admin_account
        url = self.auth_hostname + '/auth/login'
        resp = requests.post(url=url, json=data)
        if resp.ok:
            token = resp.json()['data']['accessToken']
            self.admin_token = token
            self.admin_uuid = resp.json()['data']['user']['uuid']
            self.admin_headers = {
                'Authorization': f'Bearer {token}'
            }

    def get_tokens(self):
        self.users = []
        for account in self.accounts:
            data = account
            url = self.auth_hostname + '/auth/login'
            resp = requests.post(url=url, json=data)
            if resp.ok:
                token = resp.json()['data']['accessToken']
                self.users.append({
                    'token': token,
                    'uuid': resp.json()['data']['user']['uuid'],
                    'headers': {
                        'Authorization': f'Bearer {token}'
                    },
                })

        print(len(self.users))

    def create_node(self):
        url = self.hostname + '/machinelearningnodes'
        data = {
            "name": "Node 1",
            "hostname": "http://localhost:9090",
            "description": "nothing"
        }
        resp = requests.post(url=url, headers=self.admin_headers, json=data)
        if resp.ok:
            print('ok')

    def create_workspace(self):
        url = self.hostname + '/workspaces'
        data = {
            'name': 'W 1'
        }

        resp = requests.post(url=url, json=data, headers=self.admin_headers)
        if resp.ok:
            self.workspace_id = resp.json()['data']['id']
            print(self.workspace_id)

    def create_admin_workspace_user(self):
        url = f'{self.hostname}/workspaces/{self.workspace_id}/assignuser'
        data = {
            'userUUID': self.admin_uuid,
            'role': 'Admin'
        }
        resp = requests.post(url=url, headers=self.admin_headers, json=data)
        if resp.ok:
            print('admin ok')

    def create_workspace_users(self):
        for user in self.users:
            url = f'{self.hostname}/workspaces/{self.workspace_id}/assignuser'
            data = {
                'userUUID': user['uuid'],
                'role': 'Admin'
            }
            resp = requests.post(
                url=url, headers=self.admin_headers, json=data)
            if resp.ok:
                print('annotator ok')

    def import_image(self):
        url = f'{self.hostname}/uploadimages/importurls'
        data = {
            "imageURLs": [
                "https://minio.s3.mismart.ai/trung-mua/label-studio/1.jpeg",
                "https://minio.s3.mismart.ai/trung-mua/label-studio/2.jpeg",
                "https://minio.s3.mismart.ai/trung-mua/label-studio/3.jpg",
                "https://minio.s3.mismart.ai/trung-mua/label-studio/4.jpeg",
                "https://minio.s3.mismart.ai/trung-mua/label-studio/5.jpg",
                "https://minio.s3.mismart.ai/trung-mua/label-studio/6.jpg",
                "https://minio.s3.mismart.ai/trung-mua/label-studio/7.jpg",
            ]
        }
        resp = requests.post(url=url, json=data, headers=self.admin_headers)
        if resp.ok:
            self.image_ids = resp.json()['data']
            print(f'upload {self.image_ids}')

    def create_project(self):
        url = f'{self.hostname}/projects'
        data = {
            "name": "Node 1",
            "description": "nothing",
            "projectType": "ImageClassification",
            "settings": {
                    "classes": [
                        "dog",
                        "cat"
                    ]
            },
            "hyperParameters": {
                "learningRate": 0.3,
                "batchSize": 32,
                "epochs": 3
            }
        }
        resp = requests.post(url=url, headers=self.admin_headers, json=data)
        if resp.ok:
            self.project_id = resp.json()['data']['id']
            print(f'create project {self.project_id}')

    def create_tasks(self):
        url = f'{self.hostname}/projects/{self.project_id}/projecttasks'
        data = {
            "uploadImageIDs": self.image_ids
        }
        resp = requests.post(url=url, headers=self.admin_headers, json=data)
        if resp.ok:
            self.task_ids = resp.json()['data']
            print('task ids:', self.task_ids)

    def annotate_tasks(self):
        for index, task_id in enumerate(self.task_ids):
            for user in self.users:
                rand_int = random.randint(0, 1)
                rand_int = 0 if index < 4 else 1
                # rand_null = random.randint(0, 1)
                rand_null = 0
                class_name = "cat" if rand_int == 0 else 'dog'
                data = {
                    "value": {
                        "targetClassName": class_name
                    } if rand_null == 0 else None
                }
                url = f'{self.hostname}/projects/{self.project_id}/projecttasks/{task_id}/updateannotation'
                resp = requests.post(url=url, json=data,
                                     headers=user['headers'])
                if resp.ok:
                    print(f'annotate ok {class_name}')
                else:
                    print(f'annotate not ok')


if __name__ == '__main__':
    tester = LabelStudioTester()
    tester.create_node()
    tester.import_image()
    tester.create_workspace()
    tester.create_admin_workspace_user()
    tester.create_workspace_users()
    tester.create_project()
    tester.create_tasks()
    tester.annotate_tasks()
