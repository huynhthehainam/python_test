from paho.mqtt.client import Client
import json
import random
import sys
import names
from multiprocessing import Process
import time
LATITUDES = [20.991331, 20.990926, 20.990418, 20.989956, 20.989868, 20.989829, 20.989617, 20.989517, 20.989414, 20.989293, 20.989195, 20.989143, 20.989189, 20.989215,
             20.989246, 20.989281, 20.989337, 20.989495, 20.989630, 20.989780, 20.989985, 20.990153, 20.990339, 20.990479, 20.990697, 20.990895, 20.991000, 20.991135]
LONGITUDES = [105.803306, 105.803625, 105.803871, 105.804079, 105.803930, 105.803878, 105.803545, 105.803379, 105.803188, 105.803000, 105.802838, 105.802641, 105.802412, 105.802256,
              105.802038, 105.801827, 105.801753, 105.801948, 105.802107, 105.802249, 105.802109, 105.802023, 105.801903, 105.802099, 105.802412, 105.802726, 105.802887, 105.803118]


def on_message(client, userdata, msg):
    print("Message received-> " + msg.topic + " " + str(msg.payload))  #


# The callback for when the client connects to the broker
def on_connect(client, userdata, flags, rc):
    # Print result of connection attempt
    print("Connected with result code {0}".format(str(rc)))
    client.subscribe("digitest/test1")  #


def test_multiple_drone():
    HOST = 'dronehub.mismart.ai'
    PORT = 1886
    ACCESS_TOKEN1 = 'ynLdsmR6LAIaVN2ggzXSEgkxOV6gbwya'
    ACCESS_TOKEN2 = 'BImaJXKgGxkuLE74ALZ8GTpJ894GiPBF'

    client1 = Client()
    client1.username_pw_set(ACCESS_TOKEN1)

    client1.connect(HOST, PORT)
    client1.loop_start()
    client1.on_connect = on_connect
    client1.on_message = on_message

    client2 = Client()
    client2.username_pw_set(ACCESS_TOKEN2)

    client2.connect(HOST, PORT)
    client2.loop_start()
    client2.on_connect = on_connect
    client2.on_message = on_message


def test_add_drone_stat():
    HOST = 'dronehub.mismart.ai'
    # HOST = '127.0.0.1'
    ACCESS_TOKEN = 'Pq5fAG0S87JcrsBR0e0AfVV4w0D57deN'
    # ACCESS_TOKEN = 'tgLzgGZb2NMYO6HzLGTxZYU0VWGGvsa0'
    PORT = 1886
    # PORT = 1883
    client = Client()
    client.username_pw_set(ACCESS_TOKEN)

    client.connect(HOST, PORT)
    client.loop_start()
    client.on_connect = on_connect
    client.on_message = on_message
    for i in range(3):

        data = {
            'flightDuration': random.randrange(0, 100),
            'flights': random.randrange(100, 200),
            'taskArea': random.randrange(200, 300),
            'taskLocation': 'BEN LUC, LONG AN',
            'aircraftName': 'Drone 1',
            'pilotName': 'Nam Huynh',
        }
        client.publish('Devices/me/FlightStats', json.dumps(data), 2)
        time.sleep(2)
        print(data)

    client.loop_stop()
    client.disconnect()


def test_add_telemetry():
    HOST = 'dronehub.api.mismart.ai'
    # HOST = '127.0.0.1'
    ACCESS_TOKEN = 'yS9mbGXi5c5yreFi2agb6FNPbSuDssQB'
    # ACCESS_TOKEN = 'H072550nTmw8uunD13GWuOPdskEvCMdh'
    PORT = 1886
    # PORT = 1883
    client = Client()
    client.username_pw_set(ACCESS_TOKEN)
    client.connect(HOST, PORT)
    client.loop_start()
    client.on_connect = on_connect
    client.on_message = on_message
    for i in range(len(LATITUDES)):
        data = {
            'latitude': LATITUDES[i],
            'longitude': LONGITUDES[i],
            'direction': random.randrange(0, 360),
        }
        client.publish('Devices/me/TelemetryRecords', json.dumps(data), 1)
        time.sleep(2)
        print(data)
    client.loop_stop()
    client.disconnect()


def test_add_telemetry_rev():
    LATITUDES.reverse()
    LONGITUDES.reverse()
    test_add_telemetry()


if __name__ == '__main__':
    test_add_telemetry()
