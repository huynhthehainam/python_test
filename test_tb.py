from paho.mqtt.client import Client
import json
import random
import sys
import names
import time


latitudes = [20.991331, 20.990926, 20.990418, 20.989956, 20.989868, 20.989829, 20.989617, 20.989517, 20.989414, 20.989293, 20.989195, 20.989143, 20.989189, 20.989215,
             20.989246, 20.989281, 20.989337, 20.989495, 20.989630, 20.989780, 20.989985, 20.990153, 20.990339, 20.990479, 20.990697, 20.990895, 20.991000, 20.991135]
longitudes = [105.803306, 105.803625, 105.803871, 105.804079, 105.803930, 105.803878, 105.803545, 105.803379, 105.803188, 105.803000, 105.802838, 105.802641, 105.802412, 105.802256,
              105.802038, 105.801827, 105.801753, 105.801948, 105.802107, 105.802249, 105.802109, 105.802023, 105.801903, 105.802099, 105.802412, 105.802726, 105.802887, 105.803118]

samples1 = [
    {
        'lat': 10.667356525074577,
        'lng': 105.53553301447937,
        'action': 0
    },
    {
        'lat': 10.667569635345581,
        'lng': 105.53340716277171,
        'action': 0
    },
    {
        'lat': 10.668470393848125,
        'lng': 105.53359234574434,
        'action': 0
    },
    {
        'lat': 10.66834022041227,
        'lng': 105.53570373793264,
        'action': 0
    }
]
samples2 = [
    {
        "lat": 10.655949865583352,
        "lng": 105.56631240606391,
        "action": 0
    },
    {
        "lat": 10.655771875351734,
        "lng": 105.56736843238615,
        "action": 0
    },
    {
        "lat": 10.654762013237915,
        "lng": 105.56733991624013,
        "action": 0
    },
    {
        "lat": 10.654708835812956,
        "lng": 105.56669797113786,
        "action": 0
    },
    {
        "lat": 10.654944207304224,
        "lng": 105.56668806266627,
        "action": 0
    },
    {
        "lat": 10.655010570279309,
        "lng": 105.56661847908566,
        "action": 0
    },
    {
        "lat": 10.655058048827712,
        "lng": 105.56625073949712,
        "action": 0
    }
]
# try:
#     while True:
#         i = 0
#         while i < len(latitudes):
#             sensor_data['latitude'] = latitudes[i]
#             sensor_data['longitude'] = longitudes[i]
#             sensor_data['custom_field'] = 'Created by HaiNam'
#             sensor_data['Speed'] = i*10
#             sensor_data['fuel'] = random.randrange(0, 100)
#             print(sensor_data)
#             client.publish('v1/devices/me/telemetry', json.dumps(sensor_data))
#             time.sleep(2)
#             i = i + 1
#             break

# if(i == len(Latitude)):
#     i = 0
# break
# except KeyboardInterrupt:
# pass


def upload_data(client: Client):
    coordinates = []
    for i in range(len(latitudes)):
        coordinates.append([latitudes[i], longitudes[i]])
    data = {
        'flightDuration': random.randrange(0, 100),
        'flights': random.randrange(100, 200),
        'taskArea': random.randrange(200, 300),
        'taskLocation': 'BEN LUC, LONG AN',
        'aircraftName': 'Drone 1',
        'pilotName': 'Nam Huynh',
    }
    client.publish('v1/devices/me/telemetry', json.dumps(data),1)
    time.sleep(2)
    return data


def genreate_coordinates(samples=[]):
    coordinates = []
    for sample in samples:

        coordinates.append([sample['lat'], sample['lng']])
    data = coordinates
    return data


def upload_location(client: Client):

    for i in range(len(latitudes)):
        data = {
            'latitude': latitudes[i],
            'longitude': longitudes[i],
            'speed': i*10,
            'fuel': random.randrange(0, 100),
        }
        client.publish('v1/devices/me/telemetry', json.dumps(data))
        time.sleep(2)
        print(data)
    return data


def test_tb():
    HOST = 'flighthub.mismart.ai'
    clients = []
    tokens = ['V2WLqnwVeMZt6MDdJU59']
    # tokens = ['p7a5Qy3JZ31qY9aFHHSN']
    for token in tokens:
        client = Client()
        client.username_pw_set(token)
        client.connect(HOST, 1883)
        client.loop_start()
        clients.append(client)

    try:
        total_flights = 0
        total_flight_duration = 0
        total_task_area = 0
        for client in clients:
            data = upload_data(client)
            # data = upload_location(client)
            print(data)
            # total_flights += data['flights']
            # total_flight_duration += data['flightDuration']
            # total_task_area += data['taskArea']

        print(
            f'Total flights: {total_flights}, total task area: {total_task_area}, total duration: {total_flight_duration}')

    except KeyboardInterrupt:
        pass
    for client in clients:
        client.loop_stop()
        client.disconnect()


def on_message(client, userdata, msg):
    print("Message received-> " + msg.topic + " " + str(msg.payload))  #


# The callback for when the client connects to the broker
def on_connect(client, userdata, flags, rc):
    # Print result of connection attempt
    print("Connected with result code {0}".format(str(rc)))
    client.subscribe("digitest/test1")  #


def test_local():
    HOST = '127.0.0.1'
    ACCESS_TOKEN = 'UE9dTv4tevWNCbcm0AQgcszg5JMYk6Q2'
    client = Client()

    client.connect(HOST, 1883)
    # print(f'ACCESS_TOKEN: {ACCESS_TOKEN}')
    client.username_pw_set(ACCESS_TOKEN)
    client.loop_start()
    for i in range(len(latitudes)):
        data = {
            'latitude': latitudes[i],
            'longitude': longitudes[i],

        }
        # data = {
        #     'flightDuration': random.randrange(0, 100),
        #     'flights': random.randrange(100, 200),
        #     'taskArea': random.randrange(200, 300),
        #     'taskLocation': 'BEN LUC, LONG AN',
        #     'aircraftName': 'Drone 1',
        #     'pilotName': 'Nam Huynh',
        # }
        client.publish('devices/me/TelemetryRecords', json.dumps(data), 1)
        time.sleep(2)
    client.on_connect = on_connect
    client.on_message = on_message

    client.loop_stop()
    client.disconnect()


if __name__ == '__main__':
    test_tb()
    pass
